const fetch = require('node-fetch');

let n = process.argv[2];

let start = Date.now();
for (i = 0; i < n; i++) {
    fetch('http://142.150.208.202:3000/api/Student/' + i, {
        method: 'DELETE',
    });
}

process.on('exit', () => {
    let end = Date.now();
    console.log((end - start) / 1000);
});
const fetch = require('node-fetch');

let n = process.argv[2];
let start = Date.now();
for (i = 0; i < n; i++) {
    fetch('http://142.150.208.240:3000/students/student', {
        method: 'POST',
        body: JSON.stringify({
            "studentNumber": i.toString(),
            "name": "Student Name",
            "gpa": "4.0",
            "program": "Information Technology"
        }),
        headers: {
            'Content-Type': 'application/json'
        },
    });
}

process.on('exit', () => {
    let end = Date.now();
    console.log((end - start) / 1000);
});
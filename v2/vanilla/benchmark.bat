@echo off

echo Starting experiments...
set output=C:\benchmark_vanilla_150.csv

echo Trial #, Timestamp, createStudents (Seconds), readAllStudents (Seconds), readOneRandom (Seconds), deleteStudents (Seconds) > %output%

set loop=1
:loop
echo Trial %loop% start...
(node createStudents.js) > createStudents.txt
set /p createStudents=<createStudents.txt
del createStudents.txt

(node readAllStudents.js) > readAllStudents.txt
set /p readAllStudents=<readAllStudents.txt
del readAllStudents.txt

(node readOneRandom.js) > readOneRandom.txt
set /p readOneRandom=<readOneRandom.txt
del readOneRandom.txt

(node deleteStudents.js) > deleteStudents.txt
set /p deleteStudents=<deleteStudents.txt
del deleteStudents.txt

echo %loop%, %DATE% %TIME%, %createStudents%, %readAllStudents%, %readOneRandom%, %deleteStudents% >> %output%

echo Trial %loop% complete...
set /a loop=%loop%+1 
if "%loop%"=="51" goto next
goto loop

:next
echo Done!

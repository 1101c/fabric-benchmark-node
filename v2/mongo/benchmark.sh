#!/bin/bash

# Timestamp Function
timestamp() {
    date +"%T"
}

echo "How many Objects do you want to create?"

read OBJECTS

echo "How many Trials do you want to run?"

read TRIALS

echo "Starting Experiments with $OBJECTS Objects for $TRIALS Trials..."

OUTPUT=~/benchmark_mongo_$OBJECTS.csv

echo "Trial #, Timestamp, createStudents (Seconds), readAllStudents (Seconds), readOneRandom (Seconds), deleteStudents (Seconds)" > $OUTPUT

TRIAL=0
while [  $TRIAL -lt $TRIALS ]; do
echo "Trial $(($TRIAL+1)) start..."

CREATE=$(node createStudents.js $OBJECTS)
sleep 2
READALL=$(node readAllStudents.js $OBJECTS)
sleep 2
READONERANDOM=$(node readOneRandom.js $OBJECTS)
sleep 2
DELETE=$(node deleteStudents.js $OBJECTS)
TOTALTIME=$(($(printf "%.0f" $CREATE)+$(printf "%.0f" $READALL)+$(printf "%.0f" $READONERANDOM)+$(printf "%.0f" $DELETE)))

echo "$(($TRIAL+1)), $(timestamp), $CREATE, $READALL, $READONERANDOM, $DELETE" >> $OUTPUT

echo "Trial $(($TRIAL+1)) completed in... $TOTALTIME seconds."
let TRIAL=TRIAL+1 
sleep 2
done

echo "Experiment Complete! You can find the output file at $OUTPUT"
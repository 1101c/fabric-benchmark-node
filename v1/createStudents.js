 const fetch = require('node-fetch');

 console.time("createStudent");
 for (i = 100; i < 105; i++) {
     fetch('http://localhost/students', {
         method: 'POST',
         body: JSON.stringify({
             "studentNumber": i.toString(),
             "name": "Student Name",
             "year": "Year 4",
             "email": "student@student.com",
             "gpa": "4.0",
             "program": "Information Technology"
         }),
         headers: {
             'Content-Type': 'application/json'
         },
     })
     //  .then(res => res.json())
     //  .then(json => console.log(json));
 }
 console.timeEnd("createStudent")
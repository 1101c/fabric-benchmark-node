const fetch = require('node-fetch');
const url = 'http://142.150.208.137:3000/api/Student';
const quantity = 100;

async function createStudents(_quantity) {
    console.time("Insert Objects");
    for (i = 100; i < (100 + _quantity); i++) {
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                "$class": "org.acme.biznet.Student",
                "email": "student@student.com",
                "name": "Student Name",
                "gpa": "4.0",
                "studentNumber": i.toString(),
                "program": "Information Technology",
                "year": "Year 4"
            }),
            headers: {
                'Content-Type': 'application/json'
            },
        })
        //  .then(res => res.json())
        //  .then(json => console.log(json));
    }
    console.timeEnd("Insert Objects")
}

async function getStudents() {
    console.time("Get Objects");
    await fetch(url + '/')
        // .then(res => res.json())
        // .then(json => console.log(json));
    console.timeEnd("Get Objects");
}

async function getStudent(_id) {
    console.time("Get Object");
    await fetch(url + '/' + _id)
        // .then(res => res.json())
        // .then(json => console.log(json));
    console.timeEnd("Get Object");
}

async function deleteStudents(_quantity) {
    console.time("Delete Objects");
    for (i = 100; i < (100 + _quantity); i++) {
        await fetch(url + '/' + i, {
            method: 'DELETE',
        })
    }
    console.timeEnd("Delete Objects")
}

async function runBenchmark() {
    await createStudents(quantity);
    await getStudent(100);
    await getStudent(150);
    await getStudent(200);
    await getStudents();
    await deleteStudents(quantity);
}

runBenchmark();
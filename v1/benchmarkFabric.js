const fetch = require('node-fetch');
const url = 'http://142.150.208.146:3000/students'
const quantity = 100;

async function createStudents(_quantity) {
    console.time("Insert Objects");

    for (i = 100; i < (100 + _quantity); i++) {
        await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                "studentNumber": i.toString(),
                "name": "Student Name",
                "year": "Year 4",
                "email": "student@student.com",
                "gpa": "4.0",
                "program": "Information Technology"
            }),
            headers: {
                'Content-Type': 'application/json'
            },
        })
        //  .then(res => res.json())
        //  .then(json => console.log(json));
    }
    console.timeEnd("Insert Objects")
}

async function getStudents() {
    console.time("Get Objects");
    await fetch(url + '/')
        // .then(res => res.json())
        // .then(json => console.log(json));
    console.timeEnd("Get Objects");
}

async function getStudent(_id) {
    console.time("Get Object");
    await fetch(url + '/' + _id)
        // .then(res => res.json())
        // .then(json => console.log(json));
    console.timeEnd("Get Object");
}

async function deleteStudents(_quantity) {
    console.time("Delete Objects");
    for (i = 100; i < (100 + _quantity); i++) {
        await fetch(url + '/delete/' + i)
        //  .then(res => res.json())
        //  .then(json => console.log(json));
    }
    console.timeEnd("Delete Objects")
}

async function runBenchmark() {
    await createStudents(quantity);
    await getStudent(100);
    await getStudent(150);
    await getStudent(200);
    await getStudents();
    await deleteStudents(quantity);
}

runBenchmark();